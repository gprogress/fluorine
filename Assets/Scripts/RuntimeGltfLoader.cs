/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

namespace Fluorine
{
    /// <summary>
    /// The Runtime glTF Loader implements Unity Fluorine runtime model support for the glTF 2.0 model format.
    /// glTF is a Khronos Standard implemented as a combination of JSON markup and binary buffer content.
    /// </summary>
    public class RuntimeGltfLoader : IRuntimeModelLoader
    {
        /// <summary>
        /// The list of extensions for the glTF format.
        /// This is currently ".gltf" for the conventional text format plus alternate binary buffer, ".glb" for the binary format, and ".b3dm" for the 3D Tiles variant.
        /// </summary>
        public static string[] Extensions = { ".gltf", ".glb", ".b3dm" };
        
        /// <summary>
        /// Gets the list of extensions for the glTF format.
        /// </summary>
        /// <returns>The list of glTF extensions</returns>
        public string[] GetFileExtensions()
        {
            return Extensions;
        }
        /// <summary>
        /// Loads a single glTF model file and creates a list of game objects representing it.
        /// </summary>
        /// <param name="buildInfo">A model that contains the path to the file containing the Gltf
        /// resources and the position information of the resource.</param>
        /// <returns>A model that contains the list of Unity game objects representing the Gltf 
        /// model and materials. </returns>
        public GameObjectModelTree ExecuteLoad(GameObjectModel buildInfo)
        {
            GameObjectModelTree modelTree = new GameObjectModelTree(buildInfo);
            GameObject p0 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            modelTree.model.modelGameObjects.Add(p0);
            return modelTree;
        }
    }
}