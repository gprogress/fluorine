/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

namespace Fluorine
{
    /// <summary>
    /// This class contains everything that the game objects, associated with a single path, 
    /// need to create themselves properly.  The model only needs a pathname to a file but
    /// may also contain game objects and the location information needed by that game object.  
    /// </summary>
    public class GameObjectModel
    {
        public Vector3 center { get; set; } = new Vector3(0, 0, 0);
        public string fullPath { get; private set; }
        public List<GameObject> modelGameObjects { get; set; } = new List<GameObject>();
        public GameObjectModel(string path)
        {
            fullPath = path;
        }
    }

    /// <summary>
    /// The GameObjectModelTree is designed to handle recursive loading of files (and their 
    /// associated game objects) while maintaining the parent/child hierarchy of the parent 
    /// file (often a .json file for example).  The GameObjectModelTree always needs at least 
    /// one model.  That model may have zero or many modelChildren which may have zero or many 
    /// modelChildren and so forth.
    /// </summary>
    public class GameObjectModelTree
    {
        public GameObjectModel model { get; private set; }
        public List<GameObjectModelTree> modelChildren { get; set; } = new List<GameObjectModelTree>();
        public GameObjectModelTree(GameObjectModel objectModel)
        {
            model = objectModel;
        }
    }

    /// <summary>
    /// The Runtime Model Loader interface specifies the Fluorine API for loading model files at runtime in Unity. 
    /// </summary>
    public interface IRuntimeModelLoader
    {

        /// <summary>
        /// Gets the list of known file extensions that this model loader can handle.
        /// The first extension should be the default or "preferred" extension.
        /// </summary>
        /// <returns>The list of known file extensions this model loader can handle</returns>
        public string[] GetFileExtensions();

        /// <summary>
        /// Performs a synchronous load of the given model path into a list of one or more game objects
        /// representing the model's contents and materials.
        /// </summary>
        /// <param name="buildInfo">A model that contains the absolute pathname of a file that's needed 
        /// to load the game object resources.</param>
        /// <returns>A model that contains a list of game objects or a tree of models with additional 
        /// resources that need to be loaded</returns>
        public GameObjectModelTree ExecuteLoad(GameObjectModel buildInfo);
    }
}
