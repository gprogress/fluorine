/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace Fluorine
{    
    /// <summary>
    /// The Runtime JSON Loader implements Unity Fluorine runtime model support for a JSON file format.
    /// The JSON Version supported is V1.0 of 3D tiles.
    /// </summary>        
    public class RuntimeJsonLoader : IRuntimeModelLoader
    {
        /// <summary>
        /// The base tile size from the bounding volume or the VRICON grid
        /// </summary>
        private SizeF _baseSize = new SizeF(0, 0);

        /// <summary>
        /// The center position of the tile.
        /// </summary>
        private Vector3 _baseCenter = new Vector3(0, 0, 0);

        /// <summary>
        /// The standard list of json extensions. Currently ".json".
        /// </summary>
        public static string[] Extensions = { ".json" };

        /// <summary>
        /// Gets the file extension for json.
        /// </summary>
        /// <returns>the json extension</returns>
        public string[] GetFileExtensions()
        {
            return Extensions;
        }

        /// <summary>
        /// Performs a synchronous load of the given json file into a list of one or more game objects
        /// representing the json 3D tile contents and materials.
        /// </summary>
        /// <param name="buildInfo">A model that contains the path to the file containing the 3dTile
        /// resources.</param>
        /// <returns>A model that contains the list of Unity game objects representing the 3D tile 
        /// model and materials or a quadtree of information needed to load these game objects. </returns>
        public GameObjectModelTree ExecuteLoad(GameObjectModel buildInfo)
        {
            Ogc3dTile.TilesSchema Tiles = ReadFile(buildInfo.fullPath);
            GameObjectModelTree modelTree = LoadModelTree(Tiles, buildInfo.fullPath);
            return modelTree;
        }

        /// <summary>
        /// Performs a load of the given file path and populates the TilesSchema
        /// class according to this structure.
        /// </summary>
        /// <param name="path">The path to the JSON file.</param>
        /// <returns>The populated TilesSchema that was read in from the file.</returns>
        private Ogc3dTile.TilesSchema ReadFile(string path)
        {
            JsonSerializer serializer = new JsonSerializer();
            Ogc3dTile.TilesSchema tiles;
            using (FileStream s = File.Open(path, FileMode.Open))
            using (StreamReader sr = new StreamReader(s))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                tiles = serializer.Deserialize<Ogc3dTile.TilesSchema>(reader);
            }
            return tiles;
        }

        /// <summary>
        /// This kicks off the recursive load of the tiles in the TilesSchema into the 
        /// GameObjectModelTree.  Here, we load the parent tile and then recursively load
        /// the child tiles, preserving the parent/child hierarchy.
        /// </summary>
        /// <param name="tiles">The class structure populated from the json file that 
        /// holds the tiles data</param>
        /// <param name="path">The absolute path that was passed in to load the 3D tile
        /// information, it's needed to complete the relative path of the content uris.</param>
        /// <returns>The quadtree that represents the 3D tile. </returns>
        private GameObjectModelTree LoadModelTree(Ogc3dTile.TilesSchema tiles, string path)
        {
            bool isQuadtree = SetQuadtreeSize(tiles);
            GameObjectModel singleModel = LoadSingleModel(tiles.root, path, isQuadtree);
            GameObjectModelTree modelTree = new GameObjectModelTree(singleModel);
            ExecuteRecusiveLoad(tiles.root, path, isQuadtree, modelTree);
            return modelTree;
        }

        /// <summary>
        /// We check for the VRICON_grid extension to see if this 3D tile schema is a quadtree.
        /// If it is, we will need to adjust how the size and centers are calculated for the 
        /// tiles referred to by the content uris.
        /// </summary>
        /// <param name="tiles">The current tile schema that was loaded</param>
        /// <returns>A boolean that indicated whether or not this 3D tile schema is a quadtree </returns>
        private bool SetQuadtreeSize(Ogc3dTile.TilesSchema tiles)
        {
            bool isQuadtree = false;
            if (tiles.extensionsUsed.Contains(Ogc3dTile.TileExtensions.VRICON_grid.ToString()))
            {
                List<float> vriconSize = tiles.extensions.VRICON_grid.size;
                if (vriconSize.Count == 2)
                {
                    _baseSize = new SizeF(vriconSize[0], vriconSize[1]);
                    isQuadtree = true;

                }
                List<float> vriconCenter = tiles.extensions.VRICON_grid.center;
                if (vriconCenter.Count == 2)
                {
                    _baseCenter = new Vector3(vriconCenter[0], 0, vriconCenter[1]);
                    isQuadtree = true;
                }
            }
            return isQuadtree;
        }

        /// <summary>
        /// This is the recursive load of the tiles used to populate the game objects and to 
        /// specify the relationship between the game objects.  If a tile contains sub tiles, then
        /// that tile becomes the parent of the sub tiles (children) and so on until the last sub 
        /// tile is reached.  
        /// </summary>
        /// <param name="tileRoot">The parent tile that is being loaded.</param>
        /// <param name="path">The absolute path that was passed in to load the 3D tile
        /// information, it's needed to complete the relative path of the content uris.</param>
        /// <param name="isQuadtree">This is used to complete the size and center information
        /// in the LoadSingleModel method.</param>
        /// <param name="modelTree">The parent tile that is being loaded.  If the
        /// parent tile has a child tile, then the child tile becomes the parent tile and so forth.</param>
        private void ExecuteRecusiveLoad(Ogc3dTile.Tile tileRoot, string path, bool isQuadtree, GameObjectModelTree modelTree)
        {
            foreach (var ct in tileRoot.children)
            {            
                GameObjectModel mModel = LoadSingleModel(ct, path, isQuadtree);
                GameObjectModelTree childTree = new GameObjectModelTree(mModel);
                modelTree.modelChildren.Add(childTree);
                ExecuteRecusiveLoad(ct, path, isQuadtree, childTree);
            }
        }

        /// <summary>
        /// Here we take the information from a single tile and build a single model from it that
        /// includes game objects and size information or path information to additional resources
        /// that need to be read.
        /// </summary>
        /// <param name="tile">The current tile that is being loaded</param>
        /// <param name="path">The absolute path that was passed in to load the 3D tile
        /// information, it's needed to complete the relative path of the content uris.</param>
        /// <param name="isQuadtree">This is used to complete the size and center information</param>
        /// <returns>A single model that contains the path, size and any game objects built for that model. </returns>
        private GameObjectModel LoadSingleModel(Ogc3dTile.Tile tile, string path, bool isQuadtree)
        {
            string absolutePath = GetObjectModelPath(tile.content.uri, path);
            Vector3 transform = GetObjectModelTransform(tile, isQuadtree);
            GameObjectModel gameModel = new GameObjectModel(absolutePath);

            if (absolutePath == null)
            {
                GameObject go = CreateGameObject(transform, GetObjectModelSize(tile, isQuadtree));
                gameModel.modelGameObjects.Add(go);
            }

            gameModel.center = transform;
            return gameModel;
        }

        /// <summary>
        /// Retrieves the full pathname for a given relative uri and base path.  If the uri does not exist,
        /// or is not valid, a null string is 
        /// </summary>
        /// <param name="relUri">The relative pathname contained in the 3D tile.</param>
        /// <param name="path">The absolute path that was passed in to load the 3D tile
        /// information, it's needed to complete the relative path of the uri.</param>
        /// <returns>The full, absolute path made by combining the relative URI and base path.  If the
        /// absolute path does not exist or is not valid a null string will be returned.</returns>
        private string GetObjectModelPath(string relUri, string path)
        {
            string relPath = null;

            if (relUri != null)
            {
                relPath = System.IO.Path.GetDirectoryName(path);
                Uri baseUri = new Uri(relPath + '\\');
                Uri fullPath;
                //Eventually we will handle this replace differently
                relUri = relUri.Replace(".b3dm", ".obj");
                bool isValidUri = Uri.TryCreate(baseUri, relUri, out fullPath);
                if (isValidUri && fullPath.IsFile)
                {
                    if (File.Exists(fullPath.AbsolutePath))
                    {
                        relPath = fullPath.AbsolutePath;
                    }
                    else
                    {
                        relPath = null;
                    }
                }
            }
            return relPath;
        }

        /// <summary>
        /// Gets the transform needed by the game object to position itself correctly.  In the case of
        /// a quadtree load, let L be the Length of the parent tile, and W be the width; the content uris 
        /// specified by D_R_C will have size tile length as TL= (L / pow(2, D) and tile width as TW= W / pow(2, D))  
        /// and the tile transform offset relative to its parent game object will be ((C % 2 - 0.5) * TL, 
        /// (R % 2 - 0.5) * TW.  Relative to the main base tile center, the tile center
        /// with base tile height as H and Tile length as TL horiz = H + TL(C - pow(2, D-1) )  +0.5 * TL;
        /// </summary>
        /// <param name="tile">The tile that contains the content uri and bounding volume.</param>
        /// <param name="isQuadtree">This is used to complete the size and center information
        /// in the LoadSingleModel method.</param>
        /// <returns>The transform needed by the game object to position itself correctly.</returns>

        private Vector3 GetObjectModelTransform(Ogc3dTile.Tile tile, bool isQuadtree)
        {
            Vector3 goTransform = new Vector3(0, 0, 0);
            if (isQuadtree)
            {
                SizeF size = GetObjectModelSize(tile, isQuadtree);
                int sizeFactor = ParseContentUri(tile.content.uri, 0);
                int horizFactor = ParseContentUri(tile.content.uri, 2);
                int updownFactor = ParseContentUri(tile.content.uri, 1);

                goTransform = _baseCenter;

                if (!size.IsEmpty)
                {
                    if(size.Height != 0)
                    {
                        //This first equation finds the center transform based on the size of the original parent tile at depth 0.
                        //goTransform.x = _baseCenter.x + size.Height * (horizFactor - (float)Math.Pow(2, sizeFactor - 1)) + 0.5f * size.Height;
                        //This second equation find the center transform based on the size of the parent that's just above it in depth.  
                        //This actually will not work should the parent tile be more than one level of depth above the child tile.  
                        goTransform.x = size.Height * (horizFactor % 2 - 0.5f);
                    }
                    //y is up, z north is negative, z south is positive
                    if (size.Width!= 0)
                    {
                        //goTransform.z = _baseCenter.z + size.Width * (updownFactor - (float)Math.Pow(2, sizeFactor - 1)) + 0.5f * size.Width;
                        goTransform.z = size.Width * (-(updownFactor % 2) + 0.5f);
                    }
                }
                //For the moemnt, work still needs to be done on this transform so we will return the default transform
                goTransform = new Vector3(0, 0, 0);
            }
            else
            {
                //The bounding volume is in the order (1)min long, (2)max long, (3)min lat,
                //(4)max Lat, (5)min height, (6)max height.
                if (tile.boundingVolume.region.Count >= 6)
                {
                    goTransform = new Vector3(
                        (tile.boundingVolume.region[1]-tile.boundingVolume.region[0]) / 2, 
                        0,
                        (tile.boundingVolume.region[3]-tile.boundingVolume.region[2]) / 2);
                }
            }
            return goTransform;
        }

        /// <summary>
        /// Get the size factor for quatree game objects that are not tiles created by uri's but by generic
        /// game objects created when the uri resource file is missing.  
        /// </summary>
        /// <param name="tile">The tile which contains a content uri and bounding volume.</param>
        /// <param name="isQuadtree">In a quadtree, size is relative to the tile depth, which is represented by the 
        /// first digit in the tile's content uri.</param>
        /// <returns>The size object (height and width) that was calculated from the base size and size factor, or
        /// the bounding volume height or an empty size if the sizeFactor is zero or bounding volume does not exist.</returns>
        private SizeF GetObjectModelSize(Ogc3dTile.Tile tile, bool isQuadtree)
        {
            SizeF size = new SizeF(0, 0);
            if (isQuadtree)
            {
                int sizeFactor = ParseContentUri(tile.content.uri, 0);
                if (sizeFactor != 0)
                {
                    size.Height = _baseSize.Height / (float)Math.Pow(2, sizeFactor);
                    size.Width = _baseSize.Width / (float)Math.Pow(2, sizeFactor);
                }
            }
            else if(tile.boundingVolume.region.Count >= 6 )
            {
                size.Height = tile.boundingVolume.region[5] - tile.boundingVolume.region[4];
            }
            return size;
        }

        /// <summary>
        /// Parse the given string at the given location and return the integer value.  Return 0
        /// in case of error or default.  
        /// </summary>
        /// <param name="path">The string to be parsed. </param>
        /// <param name="location">The section of the string that contains the integer value of interest.</param>
        /// <returns>The integer at the given location in the string path.  This will be 0 in
        /// case of error or default.</returns>
        private int ParseContentUri(string path, int location)
        {
            int parsedVal = 0;
            bool isValidSizeFactor = false;
            int sIndex = path.IndexOf('.');
            string strippedPath;
            strippedPath = path.Remove(sIndex);
            if (path.EndsWith(".json"))
            {
                isValidSizeFactor = Int32.TryParse(strippedPath.Split('_')[location], out parsedVal);
            }
            else
            {
                isValidSizeFactor = Int32.TryParse(strippedPath.Split('/')[location], out parsedVal);
            }
            if(!isValidSizeFactor)
            {
                parsedVal = 0;
            }
            return parsedVal;
        }

        /// <summary>
        /// Instantiates a GameObject plane with height and width obtained from the given region.  Generally, scale
        /// will be empty unless there is a quadtree, in which case, the size of the game object should be relative
        /// to the size of the other objects in the quadtree.  
        /// </summary>
        /// <param name="transformInfo">The transform that describes the transform of the game object being created.</param>
        /// <param name="scale">The height and width of the game object.</param>
        /// <returns>A game object that represents the 3D Tile. </returns>
        private GameObject CreateGameObject(Vector3 transformInfo, SizeF scale)
        {
            //The default cube is 1 unit long and 1 unit wide.  Planes are 10 units long and 
            //10 units wide by default.  When creating a cube, the y and z values should be
            //populated to immitate a tile so when rotation is applied, it also applies correctly
            //to these primitive type game objects
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.transform.position = transformInfo;
            if(!scale.IsEmpty)
            {
                go.transform.localScale = new Vector3(1, scale.Width, scale.Height);
            }
            return go;
        }
    }
}
