/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fluorine
{
    namespace Ogc3dTile
    {
        /// <summary>
        /// A list of possible Tile Extensions that can be included in the TilesSchema
        /// </summary>
        public enum TileExtensions
        {
            VRICON_grid,
            VRICON_extent
        }
        /// <summary>
        /// The TilesSchema describes a C# class representation based on the open 3D tiles JSON format V1.0.
        /// Currently asset, geometricError and root are the only required values.
        /// *Note* - This schema does not currently read 'extras' metadata.
        /// </summary>
        /// <value>
        /// The asset property contains metadata about the entire tileset.
        /// Properties describes minimum and maximum values for Long, Lat and Height
        /// The geometricError property is the error, in meters, introduced if this tileset is not rendered. 
        /// The root describes a tile in the 3D tileset.
        /// </value>
        [Serializable]
        public class TilesSchema
        {
            public Asset asset { get; set; }
            public Properties properties { get; set; }
            public float geometricError { get; set; }
            public Tile root { get; set; }
            public List<string> extensionsUsed { get; set; } = new List<string>();
            public List<string> extensionsRequired { get; set; } = new List<string>();
            public Extensions extensions { get; set; }

        }

        /// <summary>
        /// Contains metadata about the entire tileset.
        /// </summary>
        [Serializable]
        public class Asset
        {
            public string version { get; set; }
            public string tilesetVersion { get; set; }
            public Extensions extensions { get; set; }
        }

        /// <summary>
        /// Properties describes minimum and maximum values for Long, Lat and Height
        /// </summary>
        [Serializable]
        public class Properties
        {
            public MinMax Longitude { get; set; }
            public MinMax Latitude { get; set; }
            public MinMax Height { get; set; }
            public Extensions extensions { get; set; }
        }

        /// <summary>
        /// MinMax holds minimum and maximum values.
        /// </summary>
        [Serializable]
        public class MinMax
        {
            public float minimum { get; set; }
            public float maximum { get; set; }
        }

        /// <summary>
        /// Descibes a tile in a 3D Tiles tileset.
        /// </summary>
        /// <value>
        /// A bounding volume defines the spatial extent enclosing a tile or a tile's content.
        /// The viewerRequestVolume can be used for combining heterogeneous datasets, and can be combined 
        /// with external tilesets.
        /// The geometricError property is a nonnegative number that specifies the error, in meters, 
        /// of the tile's simplified representation of its source geometry.
        /// The refine property types are replacement ("REPLACE") and additive ("ADD"). If the tile has 
        /// replacement refinement, the children tiles are rendered instead of the parent.  If the tile 
        /// has additive refinement, the children are rendered in addition to the parent tile.
        /// The transform property transforms from the tile's local coordinate system to the tileset's coordinate system.
        /// Content is metadata about the tile's content and a link to the content.
        /// The children property is an array of objects that define child tiles. 
        /// </value>
        [Serializable]
        public class Tile
        {
            public BoundingVolume boundingVolume { get; set; }
            public BoundingVolume viewerRequestVolume { get; set; }
            public float geometricError { get; set; }
            public string refine { get; set; }
            public List<float> transform { get; set; } = new List<float>();
            public Content content { get; set; }
            public List<Tile> children { get; set; } = new List<Tile>();
            public Extensions extensions { get; set; }
        }

        /// <summary>
        /// Content is metadata about the tile's content and a link to the content.
        /// </summary
        [Serializable]
        public class Content
        {
            public BoundingVolume boundingVolume { get; set; }
            public string uri { get; set; }
            public Extensions extensions { get; set; }
        }

        /// <summary>
        /// A bounding volume defines the spatial extent enclosing a tile or a tile's content.
        /// </summary>
        /// <value> 
        /// The region property is an array of six numbers that define the 
        /// bounding geographic region with latitude, longitude, and height coordinates with 
        /// the order [west, south, east, north, minimum height, maximum height]. Latitudes 
        /// and longitudes are in the WGS 84 datum as defined in EPSG 4979 and are in radians. 
        /// Heights are in meters above (or below) the WGS 84 ellipsoid.
        /// The box property is an array of 12 numbers that define an oriented 
        /// bounding box in a right-handed 3-axis (x, y, z) Cartesian coordinate system where 
        /// the z-axis is up. The first three elements define the x, y, and z values for the 
        /// center of the box. The next three elements (with indices 3, 4, and 5) define the 
        /// x-axis direction and half-length. The next three elements (indices 6, 7, and 8) 
        /// define the y-axis direction and half-length. The last three elements (indices 9, 10, 
        /// and 11) define the z-axis direction and half-length.
        /// The sphere property is an array of four numbers that define a bounding 
        /// sphere. The first three elements define the x, y, and z values for the center of the 
        /// sphere in a right-handed 3-axis (x, y, z) Cartesian coordinate system where the z-axis 
        /// is up. The last element (with index 3) defines the radius in meters.
        /// </value>
        [Serializable]
        public class BoundingVolume
        {
            public List<float> region { get; set; } = new List<float>();
            public List<float> box { get; set; } = new List<float>();
            public List<float> sphere { get; set; } = new List<float>();
            public Extensions extensions { get; set; }
        }

        /// <summary>
        /// A Transform is a 4x4 affine transformation matrix, stored in column-major order, 
        /// that transforms from the tile's local coordinate system to the parent tile's coordinate 
        /// system�or the tileset's coordinate system in the case of the root tile.
        /// </summary
        /// <value>
        /// The Default property is used when the transform is not defined, it is the identity matrix:
        /// </value>
        [Serializable]
        public class Extensions
        {
            public string VRICON_extent { get; set; }
            public VRICONGrid VRICON_grid { get; set; }
        }

        /// <summary>
        /// A list of information that's possible should the VRICON extension be included
        /// </summary>
        [Serializable]
        public class VRICONGrid
        {
            public List<float> center { get; set; } = new List<float>();
            public List<float> size { get; set; } = new List<float>();
            public string cs;
            public string pcs;
        }
    }

}
