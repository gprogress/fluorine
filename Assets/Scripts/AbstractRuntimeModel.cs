/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fluorine
{
    /// <summary>
    /// The Abstract Runtime Model is a Unity behavior component (Mono Behaviour) that represents dynamically loading a single 3D model
    /// file from a file path at runtime.
    /// </summary>
    abstract public class AbstractRuntimeModel : MonoBehaviour
    {
        /// <summary>
        /// Unity Editor property setting the file path of the model to load.
        /// When set, the proper loader will be located for the file, then used to load it into a list of Unity game objects underneath the
        /// game object this component is attached to.
        /// </summary>
        public string Path;

        /// <summary>
        /// Unity Editor property setting whether the runtime model should be loaded in the Editor itself.
        /// If true, the model will be loaded as soon as possible in the editor.
        /// If false, the model will only be loaded in play mode during the game.
        /// </summary>
        public bool LoadInEditor = false;

        /// <summary>
        /// The model file that is currently loaded, if any.
        /// </summary>
        protected string Loaded;

        /// <summary>
        /// The list of game objects that were loaded from the model file.
        /// </summary>
        protected List<GameObject> Parts;

        /// <summary>
        /// Globally track game objects created through this interface to deal with switches from edit to play mode.
        /// </summary>
        private static Dictionary<string, List<GameObject> > TempParts = new Dictionary<string, List<GameObject>>();

        /// <summary>
        /// Loads the model file assigned to this runtime model and attaches the list of game objects loaded from it
        /// to be children of the given game object parent.
        /// </summary>
        /// <param name="parent">The game object that should be the parent of all loaded game objects</param>
        public void Load(GameObject parent)
        {
            if (Path != Loaded)
            {
                Unload();

                if (Path != null )
                {
                    Parts = ExecuteLoad(Path);
                    Reparent(Parts, parent);
                }
                Loaded = Path;
            }
        }

        /// <summary>
        /// OnDisable is called when the script is unchecked by the user or when switching between editor and play modes.
        /// </summary>
        public void OnDisable()
        {
            //If play mode is disabled, store the game parts that we need to keep track of before they zero out.
            if(Application.isPlaying  && !TempParts.ContainsKey(Path))
            {
                TempParts.Add(Loaded, Parts);
            }
        }

        /// <summary>
        /// Called when the script is attached to a game object or when play mode is entered.  Or when play mode is stopped
        /// and editor mode is entered.  
        /// </summary>
        public void OnEnable()
        {
            //Once play mode is disabled, the editor mode is enabled and we need to re-gain ownership of the
            //game objects we created.  
            if (!Application.isPlaying)
            {
                if (TempParts.ContainsKey(Path) && Parts == null)
                {
                    bool success = TempParts.TryGetValue(Path, out Parts);
                    Unload();
                    TempParts.Remove(Path);
                }
            }
        }
        /// <summary>
        /// Reparents the given list of game objects to have the given game object parent.
        /// </summary>
        /// <param name="parts">The child parts</param>
        /// <param name="parent">The parent to assign</param>
        public void Reparent(List<GameObject> parts, GameObject parent)
        {
            if (parts != null)
            {
                foreach (GameObject part in parts)
                {
                    //Should not re-parent all of the game objects, only the ones that do not already have
                    //a top-level parent.
                    if(part.transform.parent == null)
                    {                        
                        part.transform.parent = parent.transform;     
                        //Only rotate the parent game object, rotation as you see in the editor is actually Euler Angles.
                        parent.transform.eulerAngles = new Vector3(-90, 0, 180);

                        //Ensure the rotation of the child objects remain zero.  
                        part.transform.rotation = new Quaternion(0, 0, 0, 0);                
                    }
                }
            }
        }

        /// <summary>
        /// Unloads and destroys all game objects loaded from a runtime model file.
        /// </summary>
        public void Unload()
        {
            if (Parts != null)
            {            
                foreach (GameObject part in Parts)
                {
                    if (Application.isPlaying )
                    {
                        GameObject.Destroy(part);
                    }
                    else
                    {
                        GameObject.DestroyImmediate(part);
                    }
                }
                Parts = null;
            }
            //Resets the rotation on the parent object to none.
            gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            Loaded = null;
        }

        /// <summary>
        /// Executes the load of a given model file to obtain a list of Unity game objects loaded from the file.
        /// </summary>
        /// <param name="path">The path or URI to the model file</param>
        /// <returns>The list of game objects loaded from the file</returns>
        public virtual List<GameObject> ExecuteLoad(string path)
        {
            return null;
        }

        /// <summary>
        /// Applies a frame update to this Runtime Model behavior either during game play or when the editor registers a change.
        /// This will check to see if the currently assigned model has been loaded and, if not, execute the load.
        /// If LoadInEditor is enabled, this will happen even in editor mode, otherwise it will happen only in game play mode.
        /// If LoadInEditor is disabled but a model has been loaded, it will be unloaded.
        /// </summary>
        void Update()
        {
            if (Application.isPlaying || LoadInEditor)
            {
                Load(gameObject);
            }
            else if (!LoadInEditor && Parts != null)
            {
                Unload();
            }
        }
    }
}