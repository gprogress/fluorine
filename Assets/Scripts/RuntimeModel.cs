/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace Fluorine
{
    /// <summary>
    /// The Fluorine Runtime Model class is an instantiation of the Abstract Runtime Model interface for Unity component behaviors
    /// to handle any known or compatible model format in a general way, using a dictionary of loaders to select the most appropriate
    /// loader for any given path input.
    /// </summary>
    [ExecuteAlways]
    public class RuntimeModel : AbstractRuntimeModel
    {
        /// <summary>
        /// Default shared dictionary of file extensions (including '.' prefix) to known loaders.
        /// </summary>
        private static Dictionary<string, IRuntimeModelLoader> DefaultLoaderForExtension;

        /// <summary>
        /// Dictionary of file extensions (including '.' prefix) to known loaders.
        /// </summary>
        /// <remarks>
        /// Multiple extensions may reference the same loader.
        /// Multiple Runtime Models may use the same dictionary to share.
        /// </remarks>
        private Dictionary<string, IRuntimeModelLoader> LoaderForExtension;


        /// <summary>
        /// Adds a runtime loader to this runtime model class's dictionary.
        /// The loader's extensions (as returned by GetFileExtensions) are used as the possible file extensions.
        /// </summary>
        /// <param name="loader">The loader to add</param>
        public void AddRuntimeLoader(IRuntimeModelLoader loader)
        {
            string[] extensions = loader.GetFileExtensions();
            foreach (string ext in extensions)
            {
                LoaderForExtension.Add(ext, loader);
            }
        }

        /// <summary>
        /// Adds a runtime loader to this runtime model class's dictionary.
        /// The loader is associated with the given file extension, prefixed with the '.' character.
        /// The extension will be associated with that loader regardless of what GetFileExtensions on the loader says.
        /// </summary>
        /// <param name="ext">The file extensions to use, with the '.' prefix</param>
        /// <param name="loader">The loader to add for the given extension</param>
        public void AddRuntimeLoader(string ext, IRuntimeModelLoader loader)
        {
            LoaderForExtension.Add(ext, loader);
        }

        /// <summary>
        /// Construct a Runtime Model behavior. This populates the default dictionary.
        /// </summary>
        public RuntimeModel()
        {
            if (DefaultLoaderForExtension == null)
            {
                DefaultLoaderForExtension = new Dictionary<string, IRuntimeModelLoader>();
                LoaderForExtension = DefaultLoaderForExtension;

                this.AddRuntimeLoader(new RuntimeObjLoader());
                this.AddRuntimeLoader(new RuntimeGltfLoader());
                this.AddRuntimeLoader(new RuntimeFltLoader());
                this.AddRuntimeLoader(new RuntimeJsonLoader());
            }
            else
            {
                LoaderForExtension = DefaultLoaderForExtension;
            }
        }

        /// <summary>
        /// Gets the runtime model loader most appropriate for the given file or URI path.
        /// </summary>
        /// <param name="path">The file or URI path</param>
        /// <returns>the runtime model loader for the model type, or null if none found</returns>
        public IRuntimeModelLoader GetLoaderForPath(string path)
        {
            IRuntimeModelLoader loader = null;

            if (path != null)
            {
                string ext = System.IO.Path.GetExtension(path);
                if (ext != null)
                {
                    LoaderForExtension.TryGetValue(ext, out loader);
                }
            }

            return loader;
        }

        /// <summary>
        /// Creates the Unity game objects necessary to load the given path.  Also executes
        /// a recursive load to handle cases where the given file contains paths to other objects.
        /// </summary>
        /// <param name="path">The file or URI path</param>
        /// <returns>the list of created Unity game objects</returns>
        public override List<GameObject> ExecuteLoad(string path)
        {
            List<GameObject> parts = loadGameObjects(new GameObjectModelTree(new GameObjectModel(path)), null);
            return parts;
        }

        /// <summary>
        /// Loads game objects from a single path or executes a recursive load that loads game
        /// objects from a tree of parent/child files while maintaining the hierarchy of these
        /// files.  
        /// </summary>
        /// <param name="modelTree">The modelTree that contains the files to be loaded
        /// <param name="gameParent">In the case of a recursive load of game objects, each game
        /// object in the model tree will have gameParent as the parent.</param>
        /// <returns>the list of created Unity game objects</returns>
        private List<GameObject> loadGameObjects(GameObjectModelTree modelTree, GameObject gameParent)
        {
            List<GameObject> returnGameObjects = new List<GameObject>();
            List<GameObject> modelObjects = new List<GameObject>();
            GameObjectModelTree returnTree = new GameObjectModelTree(new GameObjectModel(null));
            IRuntimeModelLoader loader = GetLoaderForPath(modelTree.model.fullPath);
                
            List<GameObjectModelTree> childList = modelTree.modelChildren;

            if (loader != null)
            {
                returnTree = loader.ExecuteLoad(modelTree.model);
            }
            if(modelTree.model.fullPath == null)
            {
                modelObjects = modelTree.model.modelGameObjects;
            }
            else
            {
                modelObjects = returnTree.model.modelGameObjects;
            }
                foreach (var mo in modelObjects)
                {
                    if (gameParent == null)
                    {
                        gameParent = mo;
                    }
                    else
                    {
                        mo.transform.SetParent(gameParent.transform);
                    }
                    returnGameObjects.Add(mo);
                }
                if(returnGameObjects.Count > 0 )
                {
                    gameParent = returnGameObjects.Last();
                }


                //If there are files we have not yet added, recursively load them.
                if (returnTree.model.fullPath != modelTree.model.fullPath)
                {
                    returnGameObjects.AddRange(loadGameObjects(returnTree, gameParent));
                }


                //Now load the children of the model if there are any
                foreach(var mt in childList)
                {
                    returnGameObjects.AddRange(loadGameObjects(mt, gameParent));
                }
            return returnGameObjects;
        }
    }
}
