/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using Dummiesman;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using UnityEngine;

namespace Fluorine
{
    /// <summary>
    /// The Runtime OBJ Loader implements Unity Fluorine runtime model support for the OBJ model format.
    /// OBJ is a simple plain text model format dating back to the 1980s with a long history of use in interchange.
    /// </summary>
    public class RuntimeObjLoader : IRuntimeModelLoader
    {
        /// <summary>
        /// The standard list of OBJ extensions. Currently ".obj".
        /// </summary>
        public static string[] Extensions = { ".obj"};

        /// <summary>
        /// Gets the list of standard file extensions for OBJ.
        /// </summary>
        /// <returns>the list of OBJ extensions</returns>
        public string[] GetFileExtensions()
        {
            return Extensions;
        }

        /// <summary>
        /// Loads a single OBJ model file and creates a list of game objects representing it.
        /// </summary>
        /// <param name="buildInfo">A model that contains the path to the file containing the OBJ
        /// resources and the position information of the resource.</param>
        /// <returns>A model that contains the list of Unity game objects representing the OBJ 
        /// model and materials. </returns>
        public GameObjectModelTree ExecuteLoad(GameObjectModel buildInfo)
        {
            GameObjectModelTree modelTree = new GameObjectModelTree(buildInfo);
            if (!File.Exists(modelTree.model.fullPath))
            {
                Debug.LogError("File doesn't exist. "+ modelTree.model.fullPath);
            }
            else
            {
                GameObject p0 = new OBJLoader().Load(modelTree.model.fullPath);
                //We need to rotate to account for the fact the uri's are not game engine .obj's
                //and thus the axis needs to be adjusted.
                p0.transform.Rotate(0, 90, 0);
                p0.transform.position = new Vector3(modelTree.model.center.x, 0, modelTree.model.center.z);
                modelTree.model.modelGameObjects.Add(p0);
            }
            return modelTree;
        }
    }
}
