/************************************************************************
 * Copyright 2021, Geometric Progress LLC
 *
 * This file is part of the Fluorine project.
 * Fluorine is the Flexible Library for Unity-Optimized Runtime Import
 * of Native Environments.
 *
 * Fluorine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fluorine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Fluorine.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fluorine
{   
    /// <summary>
    /// Assign this script to the main camera and make sure the GameObject you want to follow is in the
    /// targetObject.  This will then tell the camera (your perspective) to follow the targetObject.
    /// </summary>
    public class CameraFollow : MonoBehaviour
    {    
        /// <summary>
        /// The targetObject is the main GameObject the camera should focus on.
        /// </summary>
        public Transform targetObject;

        /// <summary>
        /// The number of units to move with each key press when moving the camera.
        /// </summary>
        public float unitsToMove = 10000f;

        /// <summary>
        /// The camera should always start out pointing towards the target game object.
        /// </summary>
        void Start()
        {
            if(targetObject != null )
            {
                transform.LookAt(targetObject.transform.position);
            }
        }

        /// <summary>
        /// This enables the user to move around the game scene a bit, in basic forward/backward, right/left and up/down directions.
        ///    w
        /// a  s  d
        /// W moves the camera forward, S moves the camera backwards, d moves the camera to the right and a moves the camera
        /// to the left.  The up and down arrows move the camera up, vertically or down.  
        /// </summary>
        private void Update()
        {
            if (targetObject != null)
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    targetObject.position = targetObject.position - Camera.main.transform.up * unitsToMove * Time.deltaTime;
                }
                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    targetObject.position = targetObject.position + Camera.main.transform.up * unitsToMove * Time.deltaTime;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    targetObject.position = targetObject.position + Camera.main.transform.right * unitsToMove * Time.deltaTime;
                }
                if (Input.GetKeyDown(KeyCode.A))
                {
                    targetObject.position = targetObject.position - Camera.main.transform.right * unitsToMove * Time.deltaTime;
                }
                if (Input.GetKeyDown(KeyCode.W))
                {
                    targetObject.position = targetObject.position + Camera.main.transform.forward * unitsToMove * Time.deltaTime;
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    targetObject.position = targetObject.position - Camera.main.transform.forward * unitsToMove * Time.deltaTime;
                }
            }
        }
    }
}