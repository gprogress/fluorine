# README #

Fluorine (Flexible Library for Unity-Optimized Runtime Import of Native Environments) is an open source (LGPL v3) Unity technology demonstration showing that it is possible
to load 3D geospatial tiled formats via runtime streamining into Unity.

### What is this repository for? ###

* Unity technology demonstration with C# scripts for doing runtime 3D geospatial model loading in the Unity engine
* Plans to extend the runtime loading technology to support 3D Tiles and other tileset based runtime loading
* Goal is to create reusable Unity packages available to any Unity users

### How do I get set up? ###

Currently Fluorine is purely a proof-of-concept technolgy demonstration. In the future we will support packaging the Fluorine API
to support redistribution into other Unity games.

* Install Unity (2020.2.7f1 or newer) with the associated IDE of your choice
* Load Fluorine as a Unity project
* Attach the RuntimeModel component to a game object and set the Path property to the model file to load

### Contribution guidelines ###

* C# code must follow the Microsoft C# coding standard and deploy to all Unity-supported platforms
* Currently no native (C/C++) code is approved for this effort
* All pull requests are code reviewed by Geometric Progress LLC
* Currently this is a pre-alpha technology demonstration so we're not expecting reuse yet

### Who do I talk to? ###

* Maintainer: Greg Peele at Geometric Progress LLC
* Sponsor: Kyle McCullough at University of Southern California Institute for Creative Technologies